<!DOCTYPE>
<html>
	<head>
		<link rel="stylesheet" href="style.css">
		<style>
			.error {display: block;color: #FF0000; }
		</style>
	</head>
	<body>
		<h2> Registration Form</h2>

		<form class='form-style' action="register1.php" method="post">
			Username <input type="text" name="username" value="<?php echo $username; ?>"/>
					<span class = "error"><?php echo $usernameErr;?></span>

			Student ID <input type="text" name="student id" value="<?php echo $student_id; ?>"/>
					<span class = "error"><?php echo $student_idErr;?></span>	

			Password <input type="password" name="password" value="<?php echo $password; ?>"/>
					<span class = "error"><?php echo $passwordErr;?></span>	

			<input type="submit" class='button' name='submit' value= 'Register'/>

		</form>

		<?php
		//
		session_start();
		// This is a very small sample register page. The user will fill out their information
		// on the form. When they click the submit button, the data will be inserted into the database.
		$usernameErr = "";
		$username = "";
		$student_idErr = "";
		$student_id = "";
		$passwordErr = "";
		$password = "";
		if($_POST){
			$username = $_POST['username'];
			$password = $_POST['password'];
			$student_id = $_POST['sutdent id'];
			
			if (empty($username) || strlen($username) < 4 ) 
					$usernameErr = "Username is required, at least 4 chars";
			if (empty($student_id) || strlen($student_id) !=7 ) 
						$student_idErr = 'Sorry your student id must have 7 caracters! ';
						
			if (empty($password) < 6  && strlen($password) >10) {
						$passwordErr = 'password between 6 and 10 characters';  
		}
				
			if (empty($usernameErr) && empty($student_idErr) && empty($passwordErr)) {		   
			
			try {
				$host = '127.0.0.1';
				$dbname = 'wdtest';
				$user = 'root';
				$pass = '';
			
				# MySQL with PDO_MYSQL
				
				$DBH = new PDO("mysql:host=$host;dbname=$dbname", $user, $pass);
		
				$sql = "INSERT INTO Users (username, password, student id) VALUES (?, ?, ?);";
				$sth = $DBH->prepare($sql);
				
				$sth->bindParam(1, $username);
				$sth->bindParam(2, $password);
				$sth->bindParam(3, $student_id);
				
				$sth->execute();
				$_SESSION["username"] = $username;
				$_SESSION["student id"] = $student_id;
				header('Location:  confirm1.php');
				exit();
				
				echo 'You are now registered!';
				} catch(PDOException $e) {echo $e->getMessage();} 

			}		 
		}
		?>
	</body>
</html>