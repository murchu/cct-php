<?php
/**
 * Created by PhpStorm.
 * User: dmurphy
 * Date: 07/11/17
 * Time: 22:18
 */

  // checks if DB exists
  function checkDbExists() {

  }

  // checks if table in DB exists
  function checkTableExists() {

  }

  // creates DB
  function createDb() {

  }

  // creates table
  function createTable() {

  }

  // logs in a user
  function loginUser() {

  }

  // validate a users login details against DB
  function validateUserDetails() {

  }

  // checks out a book to a user
  function checkOutBook() {

  }

  // view currently checked out books for a user
  function checkedOutBooks() {

  }

  // admin - view all checked out books
  function allCheckedOutBooks() {

  }

  // admin - all books past their due date
  function allBooksDue() {

  }

  // admin - add new book to library
  function addBookToLibrary() {

  }

  // check if  user is validated & ok to visit page
  function validUser() {

  }

  // search for book in library
  function searchForBook() {

  }

  // logout of system
  function logout() {

  }

  // checks a book back into library
  function checkInBook() {

  }

  // registers a user
  function registerUser() {

  }

  // DB tables - users, books

?>