Online library
- current books in stock
- new books that are purchased

Web-app, that allows:
- login
    - student
        - search for book
        - check out a book
        - view all checked out books & due date
    - admin
        - view all checked out books & student checked out to
        - all books that have been checked out
        - all books past their due date
        - input a new book into library
- search for books
- check out books

